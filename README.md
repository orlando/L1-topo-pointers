# L1-topo-pointers

**READING MATERIAL AND OTHER RESOURCES** 

* L1Topo paper https://cds.cern.ch/record/2714861 and references in it for other subsystems  
* Other material from here https://atlas-glance.cern.ch/atlas/speakers/publicglance/talksbyconference (look for "L1" in Talk Title field)

**L1TOPO TECHNICAL RESOURCES**

* L1Topo simulation code: https://gitlab.cern.ch/atlas/athena/tree/master/Trigger/TrigT1/L1Topo/L1TopoSimulation  
* Link of L1Topo Algorithms (more or less up to date): https://twiki.cern.ch/twiki/bin/viewauth/Atlas/L1TopoAlgorithms
* Validation twiki with information  (including recipes of how to run): https://twiki.cern.ch/twiki/bin/view/Atlas/L1TopoValidation
* Validation code in gitlab (including instructions): https://gitlab.cern.ch/atlas-trigger/l1topo/L1TopoValidation
* L1Topo code in gitlab running online: https://gitlab.cern.ch/atlas/athena/tree/master/Trigger/TrigT1/L1Topo   
* L1Topo online plots: https://atlasop.cern.ch/oncall/l1calo/L1TopoPlots.php
* Counts and plots of online histograms of already taken runs: http://cmorenom.web.cern.ch/cmorenom/hdsim_plots/index.html
* Old Hdw-Sim differences versus time (run): http://test-gerbaudo.web.cern.ch/test-gerbaudo/trigger/l1topo-hdwsim/summary/
* Link with runs that give access to online histograms (already taken runs): https://atlasdaq.cern.ch/info/mda/db/ATLAS
* EOS repositiory for L1Topo /eos/atlas/atlascerngroupdisk/trig-daq/L1Topo/ 
* **Not to be used now**, useful for simulating in athena new algorithms: https://twiki.cern.ch/twiki/bin/view/Atlas/L1TopoTriggerSimulationForDevelopers
**MENU RELATED** 

* Menu for Run 3 https://docs.google.com/spreadsheets/d/1FTxC_Cibdz6EDGRs_aStKAWiuH-1SSMCPtuFCvmBycQ/edit#gid=579178713 
* Menu tested for new L1topo boards https://its.cern.ch/jira/browse/ATR-19921 
* Run 2 menu configuration 
    * Code https://gitlab.cern.ch/atlas/athena/-/blob/release/21.1.40/Trigger/TriggerCommon/TriggerMenuXML/data/L1Topoconfig_Physics_pp_v7.xml 
    * And document https://cds.cern.ch/record/1645921/files/ATL-COM-DAQ-2014-005.pdf     

**Jira ticket query search** 

    summary ~ "L1topo*" OR description ~ "L1topo*" ORDER BY status ASC, cf[12648] ASC, lastViewed ASC 

**TRIGGER MEETINGS** 

L1Calo Joint Meeting https://indico.cern.ch/event/786622/overview 

Menu experts https://indico.cern.ch/category/4533/ 

Trigger operations daily https://indico.cern.ch/category/2661/ 

Trigger coordination https://indico.cern.ch/category/2651/ 

Trigger core software https://indico.cern.ch/category/2660/ 

Trigger general meeting https://indico.cern.ch/category/2652/ 

L1 trigger https://indico.cern.ch/category/6946/ 

L1topo commissioning meetings https://indico.cern.ch/category/551/ 

**SPECIAL MEETINGS AND TALKS**

Joerg's L1 simulation overview: https://indico.cern.ch/event/832377/

Trigger workshop 2019 https://indico.cern.ch/event/772409/overview 

Run 3 L1Calo talk in TGM https://indico.cern.ch/event/794366/ 

2018 Athena MT hackathon https://indico.cern.ch/event/768334/ 

2019 Athena MT hackathon https://indico.cern.ch/event/782945/ 

Joerg talk in L1Calo meeting https://indico.cern.ch/event/776196/ 

Joerg talk in L1Muon meeting https://indico.cern.ch/event/797060/ 

Joerg talk in Trigger General Meeting https://indico.cern.ch/event/796282/ 

**FOR CONFERENCES** 

TDAQ 2019 list https://twiki.cern.ch/twiki/bin/viewauth/Atlas/TDAQSpeakersCommittee2019 

TDAQ 2018 list https://twiki.cern.ch/twiki/bin/view/Atlas/TDAQSpeakersCommittee2018 

ATLAS Poster and template https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AtlasLHCCPosters2019 

**USEFUL RESOURCES** 

https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorial?redirectedfrom=AtlasComputing.RegularComputingTutorial 

**GIT TRICKS**

Checkout somebody else's fork https://stackoverflow.com/questions/5884784/how-to-pull-remote-branch-from-somebody-elses-repo

Remember the evil ATLAS bot https://atlassoftwaredocs.web.cern.ch/gittutorial/gitlab-fork/ 

**ART tests** 

http://test-atrvshft.web.cern.ch/test-atrvshft/ART_build/21.3/Athena/x86_64-slc6-gcc62-opt/2019-02-21T2150/TrigP1Test/ 

# email template 

atlas-trig-l1topo-algcom@cern.ch 

L1Topo commissioning meeting (DATE, TIME)


Dear all,
the next L1Topo commissioning meeting this Thursday (DATE) at 11:00am CERN time. 

We will be in room ROOM. The agenda for the meeting is 

AGENDA

Please let us know if you have any other update you would like to share. 

Best,

Katharina and Nicola

